﻿using UnityEngine;
using System.Collections;

/* TrackY.cs
 *
 * Constant tracking. (not const because we can't edit that in the
 * Unity editor) */

public class TrackY : MonoBehaviour
{
	// All we want is for this script to keep track of a single constant variable.
	// Unity should really have a better system for constants... :(
	public float y = 1.0f;
}
