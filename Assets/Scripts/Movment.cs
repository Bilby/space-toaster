﻿/* Movement.cs
 * 
 * Handles player movment, including control from the user or other. */

using UnityEngine;
using System.Collections;

// CharacterAnimation is needed, flag it as something going wrong if it is missing!
[RequireComponent(typeof(CharacterAnimation))]

public class Movment : MonoBehaviour
{
    // Declare variables & define is applicable:
	[HideInInspector] public bool moving = true;
	[HideInInspector] public bool hasControl = true;

	public float acceleration = 0.8f;
	public float maxSpeed = 1.7f;
    public float targetHeight = 1.0f;

	private float speed, friction;
	private bool isLeft;
	private Vector3 pos;
	private CharacterAnimation animator;

	void Start()
	{
        // Initialise variables:
		speed = 0.0f;
		friction = acceleration * 2.75f;

		// Initialise access to other scripts:
		animator = transform.GetComponent<CharacterAnimation>();
	}

	void Update()
	{
		// Temp key checking to avoid a moonwalking like effect:
		bool left = false, right = false;

        // Assume not moving, proof otherwise if key pressed:
		moving = false;

        // Check if user has control of player at current time:
        if (hasControl)
        {
            // Get input from keyboard:
			if (Input.GetButton("Left") || Input.GetButton("AltLeft"))
				left = true;
			else if (Input.GetButton("Right") || Input.GetButton("AltRight"))
				right = true;

			if (left && right)
				moving = false;
			else if (left)
			{
				moving = true;
				isLeft = true;
			}
			else if (right)
			{
				moving = true;
				isLeft = false;
			}
			else
				moving = false;
        }
	}

	void FixedUpdate()
	{
		// Temporary pos val:
		pos = transform.position;

		// Ensure speed is not exceeded, clamp is not working for this so this impimentation works fine...
		speed = (speed > maxSpeed) ? maxSpeed : (speed < -maxSpeed) ? -maxSpeed : speed;

		// Apply friction if no key was pressed or movment if a key was pressed:
		if (moving)
			speed = (isLeft) ? (speed - acceleration * Time.deltaTime) : (speed + acceleration * Time.deltaTime);
		else
			if (Mathf.Abs(speed) > friction * Time.deltaTime)
				speed = (speed < 0.0f) ? (speed + friction * Time.deltaTime) : (speed - friction * Time.deltaTime);
			else
				speed = 0.0f;
		
		// Send required info to our animation script:
		animator.DirLeft(isLeft);
		animator.speed = speed;
		
		// Apply final vector physics math:
		pos.x += speed * Time.deltaTime;
		pos.y = Mathf.Lerp(pos.y, targetHeight, 3.2f * Time.deltaTime);
		
		// Finalise temp value:
		transform.position = pos;
	}

    // Check for a collision with a y changing trigger:
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.name == "resetY")
			targetHeight = col.gameObject.GetComponent<TrackY> ().y;
	}
}