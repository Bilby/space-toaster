﻿using UnityEngine;
using System.Collections;

/* LevelTrigger.cs
 * 
 * This script is to be placed on a collider in order to trigger an event.
 * The event is a hash derived from a string given to name the event. */

public class LevelTrigger : MonoBehaviour
{
	// Variable decleration:
	[HideInInspector] public int idHash;

	public string id;

	void Start()
	{
		// Creates hash:
		idHash = Animator.StringToHash(id);
	}
}