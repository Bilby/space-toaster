﻿using UnityEngine;
using System.Collections;

/* RunEvent.cs
 * 
 * This script allows for different events to be scripted, without the need
 * to actually hardcode level scripting. It will allow for a variety of events
 * to be set and take place from within the Unity editor.
 * 
 * It can be noted here that efficiency is kept in mind, due to the nature of the
 * items being "spawned" they will actually get created on awake, thus the whole scene
 * can be loaded at once rather than spawning items in at runtime and causing a slowdown. */

// These are the different types of events we can use:
public enum eventTypes
{
	ET_DESTROY,
	ET_TELEPORT,
	ET_HIDE,
	ET_SHOW,
	ET_SPAWN,
	ET_CHANGESCENE,
}

// A container for held events, note not MonoBehavior:
public class EventContainer
{
	// Variables:
	public int eventID, eventTriggerID;

	// Constructor for easy passing:
	public EventContainer(int _eventID, int _eventTriggerID)
	{
		eventID = _eventID;
		eventTriggerID = _eventTriggerID;
	}
}

public class RunEvent : MonoBehaviour
{
	// Variable decleration:
	public eventTypes et;
	public string eventID;
	public string eventTriggerID;
	public string waitForEvent;
	public string waitForEventTrigger;
	public string sceneName;
	public string prefabPath;
	public string prefabName;
	public string targetName; // Only if we can't find a GameObject for target!
	public Vector3 offset;
	public Vector3 targetLocationPosition;
	public GameObject tagertLocation;
	public GameObject target;
	public float timeBeforeRunEvent = 0.0f;
	public bool waitForEventListed = false;
	public bool destroyOnDone = true;
	public bool allowSim = false; // Advanced use only!
	public bool addToCompleteListOnDone = true;
	public bool showLogs = true;

	[HideInInspector] public int eventIDHash, eventTriggerIDHash;
	[HideInInspector] public GameObject spawnObj;

	private bool hasStarted = false;
	private Vector3 realtarget;
	private SceneController sc;
	private float timer;
	private SpriteRenderer spr;
	private FastParticleEmmiter fpe;
	private DialougeController dc;
	private int waitForEventID, waitForEventTriggerID;

	void Start()
	{
		// Get Scene Controller referance:
		sc = GameObject.Find("SceneController").GetComponent<SceneController>();

		// Attach self to event list:
		sc.AttachEvent(this);

		// Create our hash's:
		eventIDHash = Animator.StringToHash(eventID);
		eventTriggerIDHash = Animator.StringToHash(eventTriggerID);
		waitForEventID = Animator.StringToHash(waitForEvent);
		waitForEventTriggerID = Animator.StringToHash(waitForEventTrigger);

		if (et != eventTypes.ET_SPAWN && target == null)
		{
			// Look for our target if it needs to be created dynamically:
			target = GameObject.Find(targetName);
		}

		// Decide upon spawn point/teleport point:
		if (et == eventTypes.ET_TELEPORT || et == eventTypes.ET_SPAWN)
			if (target == null)
				realtarget = targetLocationPosition;
			else
				realtarget = target.transform.position;

		if (et == eventTypes.ET_SPAWN)
		{
			// Find appropriate path:
			string finalPath = (prefabPath == "") ? prefabName : prefabPath + "/" + prefabName;

			// Instatiate a new Object from prefab:
			spawnObj = (GameObject)Instantiate(Resources.Load(finalPath));

			// Set it's name properly, thanks Unity...
			spawnObj.name = prefabName;

			// Get the sprite from the Object:
			spr = spawnObj.GetComponent<SpriteRenderer>();

			// Hide the Object until we are ready to "spawn" it:
			if (spr != null)
				spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 0.0f);
		}

		// Get target referances:
		if (target != null)
		{
			// Get our dialougeController:
			dc = target.GetComponent<DialougeController>();

			// Get Sprite Renderer:
			if (dc == null)
				spr = target.GetComponent<SpriteRenderer>();
		
			fpe = target.GetComponent<FastParticleEmmiter>();
		}
		else if (spawnObj != null)
			// Get our dialougeController:
			dc = spawnObj.GetComponent<DialougeController>();
	}

	void FixedUpdate()
	{
		// First we need to test if the event has begun:
		if (hasStarted)
		{
			// Then we need to test the event timer:
			if (timer <= 0.0f)
			{
				// We can run the event:
				switch (et)
				{
				case eventTypes.ET_DESTROY:
					Destroy(target);
					break;
				case eventTypes.ET_HIDE:
					// Set our Dialouge Controllers special hidden flag:
					if (dc != null)
					{
						dc.hidden = true;
						break;
					}

					if (fpe != null)
					{
						fpe.on = false;
						break;
					}

					// Set sprite alpha to 0:
					spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 0.0f);

					break;
				case eventTypes.ET_SHOW:
					// Set our Dialouge Controllers special hidden flag:
					if (dc != null)
					{
						dc.hidden = false;
						break;
					}

					if (fpe != null)
					{
						fpe.on = true;
						break;
					}

					// Set sprite alpha to 1:
					spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 1.0f);

					break;
				case eventTypes.ET_SPAWN:
					// Show the sprite:
					if (dc != null)
						dc.hidden = false;
					else
						spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, 1.0f);

					// teleport our new found friend:
					spawnObj.transform.position = realtarget;

					break;
				case eventTypes.ET_TELEPORT:
					// Teleport:
					target.transform.position = realtarget;

					break;
				default:
					// Just in case:
					Debug.LogError("Something has gone very wrong!");

					break;
				}

				// Finish up:
				hasStarted = false;

				// Tell Scene controller we are done:
				sc.EventEnded(eventTriggerIDHash, eventIDHash);

				if (showLogs)
					Debug.Log("Event of trgger: " + eventTriggerIDHash + ", and ID: " + eventIDHash + " Has finished.");
			}
			else
			{
				// Update timer:
				timer -= Time.deltaTime;
			}
		}
	}

	public void begin()
	{
		// Test if we are already running this event:
		if (hasStarted)
			return;

		// Test if we can run the event:
		if (!allowSim)
			if (sc.EventProcessing(eventTriggerIDHash, eventIDHash))
				return;

		// Test if we have already run this event, and if we can run it again:
		if (destroyOnDone)
			if (sc.EventDone(eventTriggerIDHash, eventIDHash))
				return;

		// Test if we are waiting on another script to end:
		if (waitForEventListed)
			if (!sc.EventDone(waitForEventTriggerID, waitForEventID))
			    return;

		// Begin the event:
		hasStarted = true;

		// Add this event to the processing list:
		sc.EventStarted(eventTriggerIDHash, eventIDHash);

		// Set timer:
		timer = timeBeforeRunEvent;

		// Debug:
		if (showLogs)
			Debug.Log("A new event of trgger: " + eventTriggerIDHash + ", and ID: " + eventIDHash + " Has begun processing.");
	}
}
