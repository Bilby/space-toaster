﻿using UnityEngine;
using System.Collections;

/* CharacterAnimation.cs
 *
 * Handles things to do with character sprites.
 * Can be attached to non character objects but will only handle lighting levels. */

// Requires a Sprite Renderer.
[RequireComponent(typeof(SpriteRenderer))]

public class CharacterAnimation : MonoBehaviour
{
	// Initialise Variables:
	[HideInInspector] public float speed;

	public float lightLevel = 1.0f;
	public bool canFlip = true;

	private Animator anim;
	private SpriteRenderer spr;
	private int hash;

	void Start()
	{
		// Gets the animation controler and a hash referance to "Speed".
		// Doing this greatly improves efficiency.
		anim = GetComponent<Animator>();
		spr = GetComponent<SpriteRenderer>();
		hash = Animator.StringToHash("Speed");
	}
	
	void Update()
	{
		// Sets lighting level:
		spr.color = new Color(lightLevel, lightLevel, lightLevel * 2.0f, spr.color.a);

		// Sets the speed variable, this determines weather to animate at all or not:
		if (anim)
			anim.SetFloat(hash, Mathf.Abs(speed));
	}

	public void DirLeft(bool isTrue)
	{
		// This function is for other scripts to call, to tell
		// this script it's direction.
		if (canFlip)
		{
			Vector3 scale = transform.localScale;

			scale.x = (isTrue) ?
					  (scale.x > 0.0f) ? (scale.x * -1.0f) : scale.x:
					  (scale.x < 0.0f) ? (scale.x * -1.0f) : scale.x;

			transform.localScale = scale;
		}
	}
}
