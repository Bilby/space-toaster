﻿using UnityEngine;
using System.Collections;

/* FastParticleEmmiter.cs
 * 
 * because I CBF using Unity's particle emmiter for certain things
 * like the door knocking in level 1. */

public class FastParticleEmmiter : MonoBehaviour
{
	public bool on = false;
	public string[] prefabNames = new string[1];
	public float waitTime = 5.6f;

	private float t;

	void Awake()
	{
		t = waitTime;
	}

	void FixedUpdate()
	{
		// If turned off do not emit:
		if (!on)
			return;

		if (t > 0.0f)
		{
			t -= Time.deltaTime;
			return;
		}

		// Start emmiting:
		int r = (int)Random.Range(0, 5);

		for (int i = 0; i < r; ++i)
		{
			int j = 0;
			bool selected = false;

			foreach(string s in prefabNames)
			{
				int rj = (int)Random.Range(0, 10);

				if (rj > 3)
				{
					selected = true;
					break;
				}
				else
					++j;
			}

			if (selected)
			{
				GameObject g = (GameObject)Instantiate(Resources.Load(prefabNames[j]));

				FastParticle fp = g.GetComponent<FastParticle>();

				g.transform.position = transform.position;
				g.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Random.Range(0.0f, 360.0f));

				fp.speed = Random.Range(1.75f, 3.56f);
				fp.fadeSpeed = Random.Range(1.3f, 6.78f);
			}
		}

		t = waitTime;
	}
}
