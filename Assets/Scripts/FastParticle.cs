﻿using UnityEngine;
using System.Collections;

/* FastParticle.cs
 * 
 * A very minimal "Particle" */

public class FastParticle : MonoBehaviour
{
	// Variable decleration:
	public float fadeSpeed = 2.56f;
	public float speed = 4.78f;
	public float alpha = 1.0f;

	private SpriteRenderer spr;

	void Start()
	{
		// Get components:
		spr = GetComponent<SpriteRenderer>();
	}

	void FixedUpdate()
	{
		//Check for destruction:
		if (alpha <= 0.05f)
		{
			Destroy(transform.gameObject);
			return;
		}

		// Set colour:
		spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);

		// Set alpha:
		alpha = Mathf.Lerp(alpha, 0.0f, fadeSpeed * Time.deltaTime);

		// Update position:
		Vector2 p = new Vector2(transform.position.x, transform.position.y);

		p.x += Mathf.Sin(transform.rotation.z) * speed * Time.deltaTime;
		p.y += Mathf.Cos(transform.rotation.z) * speed * Time.deltaTime;

		transform.position = new Vector3(p.x, p.y, transform.position.z);
	}
}
