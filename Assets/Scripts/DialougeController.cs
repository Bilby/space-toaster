﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/* DialougeController.cs
 * 
 * Keeps track of the speech bubbles and ensures that
 * they stay in the screen space / keep text aligned
 * and other such things. 
 *
 * The good news with all of this processing, is that it is all
 * pre baked in the Awake function, so you can do it in a load screen
 * without causing any slowdowns on the game itself. */

// Requires a Sprite Renderer & GUIText:
[RequireComponent(typeof(SpriteRenderer))]

public class DialougeController : MonoBehaviour
{
	// Set up variables:
	public string colourTag = "character name here!";
	public string text = "copy dialouge here!#This is a really long string that will get auto seperated by the script. If you want a new line, insert a #";
	public Color customColour = new Color(1.0f, 1.0f, 1.0f, 0.8f);
	public int maxLines = 7, maxCharWidth = 23, fontSize = 36, textGap = 50;
	public float fadeSpeed = 2.4f, flipOffset = 6.2f;
	public bool hidden = false;
	public bool destroyOnHidden = true;
	public Font font;
	public Vector2 offset = new Vector2(1.7f, 9.3f);
	public GameObject attach;
	public string attachName; // Only for if the Object is spawned by an event controller!

	private List<string> lines = new List<string>();
	private float left, right, top, bot, alpha;
	private int strSize, Zee, Clem, Antony, villager1, villager2, villager3, villager4;
	private SpriteRenderer spr;
	private GameCamera localCamera;
	private GUIStyle style;
	private bool beenSeen = false;

	void Awake()
	{
		// Make hash of colour tag:
		int hashCompare = Animator.StringToHash(colourTag);

		// Get our ID bank ready:
		InvolkeHashes();

		// Set alpha to hidden value:
		alpha = (hidden) ? 0.0f : 0.8f;

		// Set the colour:
		customColour = GetColour(hashCompare);

		// Initialise strSize:
		strSize = text.Length;

		// Temporary variables for loop to allocate sub strings as lines:
		char[] temp = text.ToCharArray();
		int lastSpace = -1, i = 0;
		bool endOfArray = false;

		// Keep going until end of string or max lines reached.
		while (!endOfArray && lines.Count < maxLines)
		{
			// Working line until final is ready:
			char[] tempLine = new char[maxCharWidth+1];

			// Set the first line of the array to NULL termination char:
			tempLine[0] = '\0';

			// Loop over lines:
			for (int j = 0; j < maxCharWidth+1; ++j)
			{
				// Test for end of string:
				if (i+j > strSize-1)
				{
					endOfArray = true;
					break;
				}

				// Test for a new line char:
				if (temp[i+j] != '#')
				{
					// Set last space variable:
					if (temp[i+j] == ' ')
					{
						if (j == 0)
							break;

						if (j != maxCharWidth)
								lastSpace = i+j;
						else
						{
							i += j;
							break;
						}
					}
					
				    tempLine[j] = temp[i+j];
				}
				else
				{
					i += j;
					break;
				}
				// Test is we are on the last possible char of a line:
				if (j == maxCharWidth)
				{
					// If the next char is a space:
					if (temp[i+j+1] == ' ')
					{
						i += j + 1;
						break;
					}

					// If our space is close enough to break the word:
					if (lastSpace != -1 && (i + j - lastSpace) < (maxCharWidth/2))
					{
						// Terminate last space:
						for (int k = lastSpace-i; k < maxCharWidth+1; ++k)
							tempLine[k] = '\0';

						// Reset i counter:
						i = ++lastSpace;

						break;
					}
					else
					{
						// Put in a hyphon to mark word continuing on next line:
						tempLine[j] = '-';

						i+=j;
						break;
					}
				}
			}

			// If we got anything:
			if (tempLine[0] != '\0')
				// Set up curent line:
				lines.Add(new string(tempLine));
			else
				// Push forwards i:
				++i;
		}

		// Throw Error:
		if (!endOfArray)
			Debug.Log("WARNING: The given text may be too long!");
	}

	void Start()
	{
		// Set our referance to SpriteRenderer:
		spr = GetComponent<SpriteRenderer>();

		// Find the game localCamera:
		GameObject[] allObjects = Object.FindObjectsOfType(typeof(GameObject)) as GameObject[];

		foreach (GameObject i in allObjects)
		{
			// Sift through every game object in the scene hunting for the localCamera:
			localCamera = i.GetComponent<GameCamera>();
			if (localCamera != null)
				break;
		}
		
		// Set the Sprite colour to one picked previously:
		spr.color = customColour;

		// Set the scale to 0.7:
		transform.localScale = new Vector3(transform.localScale.x * 0.7f, transform.localScale.y * 0.7f, 1.0f);

		// Set up GUI style:
		style = new GUIStyle();

		// Set font to use:
		style.font = font;

		// Set font size:
		style.fontSize = fontSize;

		// Find our attach Object if one has not been automatically:
		if (attach == null)
			attach = GameObject.Find(attachName);
	}

	void FixedUpdate()
	{
		// Temp variables:
		alpha = spr.color.a;

		//Check transparency:
		if (hidden)
			alpha = (alpha > 0.0f) ? Mathf.Lerp(alpha, 0.0f, fadeSpeed * Time.deltaTime) : alpha;
		else
			alpha = (alpha < 0.8f) ? Mathf.Lerp(alpha, 0.8f, fadeSpeed * Time.deltaTime) : alpha;

		spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);
		
		// Adjust scale:
		if (attach != null)
		{
			transform.position = attach.transform.position;

			bool makeSwitch = false;

			if (attach.transform.localScale.x < 0.0f)
			{
				if (transform.localScale.x > 0.0f)
				{
					makeSwitch = true;
					offset.x -= flipOffset;
				}
			}
			else if (attach.transform.localScale.x > 0.0f)
			{
				if (transform.localScale.x < 0.0f)
				{
					makeSwitch = true;
					offset.x += flipOffset;
				}
			}

			if (makeSwitch)
			{
				// Make a temp vector:
				Vector3 _scale = transform.localScale;

				// Switch x axis:
				_scale.x *= -1.0f;

				// Set it back to scale:
				transform.localScale = _scale;
			}
		}

		// Set our destructor flag if we need:
		if (destroyOnHidden)
			if (alpha < 0.05f)
			{
				if (beenSeen)
					Destroy(transform.gameObject);
			}
			else
			{
				beenSeen = true;
			}
	}

	void OnGUI()
	{
		// Set shade color value:
		style.normal.textColor = new Color(0.2f, 0.2f, 0.2f, alpha);

		// Get start point (top left):
		Vector2 start = new Vector2(transform.position.x, transform.position.y);

		// Calculate offset:
		start += offset;

		// Calculate screen coordinates:
		start = localCamera.GameToScreenCoords(start);

		// Iterate adding to our coords:
		int y = 0;

		for (int i = 0; i < lines.Count; ++i)
		{
			GUI.Label(new Rect(start.x, start.y+y, start.x, start.y+y), lines[i], style);
			y += textGap;
		}

		// Set regular color value:
		style.normal.textColor = new Color(1.0f, 1.0f, 1.0f, alpha);

		// Set back our coords:
		start -= new Vector2(1.0f, 1.0f);

		// Iterate adding to our coords:
		y = 0;

		for (int i = 0; i < lines.Count; ++i)
		{
			GUI.Label(new Rect(start.x, start.y+y, start.x, start.y+y), lines[i], style);
			y += textGap;
		}
	}

	void InvolkeHashes()
	{
		// Compose Hash's:
		Zee = Animator.StringToHash("Zee");
		Clem = Animator.StringToHash("Clem");
		Antony = Animator.StringToHash("Antony");
		villager1 = Animator.StringToHash("villager1");
		villager2 = Animator.StringToHash("villager2");
		villager3 = Animator.StringToHash("villager3");
		villager4 = Animator.StringToHash("villager4");
	}

	Color GetColour(int hash)
	{
		/* Unity won't let you do a switch statement with non constants,
		 * to be fair, not many other languages can either, so instead enjoy this
		 * nicely chained turnery operator :3 */
		return (hash == Zee) ? new Color(0.43137f, 0.8117647f, 1.0f, alpha) :
			   (hash == Clem) ? new Color(0.380392f, 0.38039215f, 1.0f, alpha) :
			   (hash == Antony) ? new Color(1.0f, 0.3803924f, 0.709803921f, alpha) :
			   (hash == villager1) ? new Color(0.60392156f, 0.380392156f, 0.4196078431f, alpha) :
			   (hash == villager2) ? new Color(1.0f, 0.380392156f, 1.0f, alpha) :
			   (hash == villager3) ? new Color(1.0f, 0.564705882f, 0.380392156f, alpha) :
			   (hash == villager4) ? new Color(1.0f, 0.760784313f, 0.380392156f, alpha) :
			   customColour;
	}
}