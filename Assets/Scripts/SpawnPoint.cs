﻿using UnityEngine;
using System.Collections;

/* SpawnPoint.cs
 * 
 * This gives a public Vector for the spawn point.
 * You can access this via spawn point list for the Scene Controller. */

public class SpawnPoint : MonoBehaviour
{
	// The spawn point position, this is defined in Start():
	[HideInInspector] public Vector2 pos;
	[HideInInspector] public string id;

	void Start()
	{
		// Define the pos variable to be the x and y coordinates from the GameObject position:
		pos.x = transform.position.x;
		pos.y = transform.position.y;

		// Define Spawn Point ID:
		id = name;

		// Debugging:
		if (Debug.isDebugBuild)
		{
			if (!id.StartsWith("sp_"))
				Debug.Log("Warning: Spawnpoint " + id + "Does not start with \"sp_\" as it's GameObject Name! This will mean it probably won't work as a spawn point!");
		}
	}

	// Teleports a given object to this spawn point:
	public void teleportObject(GameObject o)
	{
		// Damn Unity making you use temporary Vectors...
		Vector3 temp = o.transform.position;
		temp.x = pos.x;
		temp.y = pos.y;
		o.transform.position = temp;

		// Log the teleport:
		Debug.Log(o.name + ": teleported to (" + pos.x + "," + pos.y + ").");
	}
}