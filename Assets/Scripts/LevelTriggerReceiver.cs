﻿using UnityEngine;
using System.Collections;

/* LevelTrigger.cs
 * 
 * This script is to be placed on an Object that we wish to hit a trigger.
 * Upon hitting a trigger, this Objects id, and the id for the event will be
 * passed to the SceneController for processing. */

public class LevelTriggerReceiver : MonoBehaviour
{
	// Variable decleration:
	[HideInInspector] public int idHash;

	public string id;

	private SceneController sc;

	void Start ()
	{
		// Creates hash:
		idHash = Animator.StringToHash(id);

		// Find SceneController:
		sc = GameObject.Find("SceneController").GetComponent<SceneController>();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		// Get the LevelTrigger if there is one:
		LevelTrigger lt = col.GetComponent<LevelTrigger>();

		// If there is one then pass the event to the scene controller:
		if (lt != null)
			sc.SendEvent(idHash, lt.idHash);
	}
}
