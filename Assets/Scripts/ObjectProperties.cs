﻿using UnityEngine;
using System.Collections;

/* ObjectProperties.cs
 * 
 * The idea here is that every object has certain properties that need
 * to be easily set within the Unity editor and be easily called from
 * other scripts such as the GameController ChangeScene function. */

public class ObjectProperties : MonoBehaviour
{
	public bool destroyOnSceneChange = true;

	void Awake()
	{
		// Set up not being destroyed on Load:
		if (!destroyOnSceneChange)
			DontDestroyOnLoad(transform.gameObject);
	}
}