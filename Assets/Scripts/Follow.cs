﻿using UnityEngine;
using System.Collections;

/* Follow.cs
 * 
 * Has a distance and a target, will keep characters walking within a certain distance of object it's following.
 * Also controls animator and y gradient for character. */

// CharacterAnimation is needed, flag it as something going wrong if it is missing!
[RequireComponent(typeof(CharacterAnimation))]

public class Follow : MonoBehaviour
{
    // Initialise variables:
    public float minDist = 1.0f;
    public float moveSpeed = 3.0f;
    public float targetHeight = 1.0f;
    public string targetName = "";
    private bool isLeft = false;
    
    // Access to contoller scripts:
    private CharacterAnimation animator;
	private SpriteRenderer spr;
	private GameObject target;

    void Start()
    {
        // Initialise access to other scripts:
        animator = transform.GetComponent<CharacterAnimation>();

		spr = GetComponent<SpriteRenderer>();

		target = GameObject.Find(targetName);
    }

	void Update()
    {
		// Check if we should follow:
		if (spr.color.a == 0.0f)
			return;

        // Set animation to nothing:
        animator.speed = 0.0f;

        // Get distance and temp variables:
        float x = transform.position.x;
        float tx = x;
        Vector3 ret = transform.position;

        // Don't do anything we don't have to...
        if (target != null)
        {
            tx = target.transform.position.x;

            // Work distance:
            float dist = tx - x;

            // Move towards the target:
            isLeft = (dist < 0.0f);

            // Send required info to our animation script:
            animator.DirLeft(isLeft);

            // If the target is in range we don't need to do anything.
            if (Mathf.Abs(dist) > minDist)
            {
                // Send more required info to our animation script:
                animator.speed = 0.5f;

                // Lerp to the position:
                ret = Vector3.Lerp(transform.position, new Vector3(tx, targetHeight, transform.position.z), moveSpeed * Time.deltaTime);
            }
        }

        // Finalise:
        transform.position = ret;
    }

    // Check for a collision with a y changing trigger:
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.name == "resetY")
        {
            targetHeight = col.gameObject.GetComponent<TrackY>().y;
        }
    }
}
