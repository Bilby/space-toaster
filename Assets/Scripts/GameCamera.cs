﻿/* Movement.cs
 * 
 * Handles the in game camera, in it's relation to whatever object it
 * should be looking at, this will most likely be the player or
 * some object of intrest, and can be 'attached' to any Unity GameObject
 * lerping to it's position. */

using UnityEngine;
using System.Collections;

// Require a camera:
[RequireComponent(typeof(Camera))]

public class GameCamera : MonoBehaviour
{
    // Declare variables & define is applicable:
    public GameObject lookAt = null;
    public float followSpeed = 4.2f;
    public float height = 4.2f;
	//public float bloomStrength = 0.5f;
	//public Material bloomShader;

    private Vector3 target;
	private Camera cam;
	//private RenderTexture rt;

	void Start()
	{
		// Get camera component:
		cam = GetComponent<Camera>();

		//rt = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.Default);
	}

	void FixedUpdate()
    {
	   	// Update lookAt target:
        Vector3 target = lookAt.transform.position;
        target.z = transform.position.z;
        target.y += height;

        // Perform Lerp:
        transform.position = Vector3.Lerp(transform.position, target, followSpeed * Time.deltaTime);
	}

	// Called by Camera component:
	/*void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		// Do shader stuff if it is supported:
		if (!SystemInfo.supportsImageEffects)
			if (bloomShader == null || !bloomShader.shader.isSupported)
				return;

		// Send our strength component to the shader:
		bloomShader.SetFloat("_strength", bloomStrength);

		// Send our new shaded source back to the renderer:
		Graphics.Blit(source, rt, bloomShader, 0);
		Graphics.Blit(rt, destination);

		Debug.Log ("Called");
	}*/

	public Vector2 GameToScreenCoords(Vector2 coords)
	{
		// Convert Game Coordinates to screen coordinates based on camera position:
		Vector3 ret = cam.WorldToScreenPoint(new Vector3(coords.x, coords.y, 0.0f));

		// This flips the y axis for no reason at all, because FUCK EVERYTHING...
		// Thanks for that Unity...
		// Oh yeah what this line does, oh it flips the y axis back to what it is meant to be:
		ret.y = Screen.height - ret.y;

		// Finally return this abomination, not before turning it back into a Vector2!
		// See why I hate Unity yet?
		return new Vector2(ret.x, ret.y);
	}
}
