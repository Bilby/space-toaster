﻿using UnityEngine;
using System.Collections;

/* MouseTrigger.cs
 * 
 * Allows a mouse click to enable a trigger. */

// Type list for reducing lag:
public enum typeList
{
	M_DOWN,
	M_UP,
	M_OVER,
}

[RequireComponent(typeof(BoxCollider))]

public class MouseTrigger : MonoBehaviour
{
	// Variable decleration:
	public string eventID = "Unique ID here.";
	public typeList type = typeList.M_DOWN;

	private int eventIDHash, triggerHash, triggerUpHash, triggerOverHash;
	private SceneController sc;

	void Start()
	{
		// Get ID Hash codes:
		eventIDHash = Animator.StringToHash(eventID);
		triggerHash = Animator.StringToHash("MouseDown");
		triggerUpHash = Animator.StringToHash("MouseUp");
		triggerOverHash = Animator.StringToHash("MouseOver");

		// Get scene controller:
		sc = GameObject.Find("SceneController").GetComponent<SceneController>();
	}

	// Send triggers:
	void OnMouseDown()
	{
		if (type == typeList.M_DOWN)
			sc.SendEvent(triggerHash, eventIDHash);
	}

	void OnMouseUp()
	{
		if (type == typeList.M_UP)
			sc.SendEvent(triggerUpHash, eventIDHash);
	}

	void OnMouseOver()
	{
		if (type == typeList.M_OVER)
			sc.SendEvent(triggerOverHash, eventIDHash);
	}
}
